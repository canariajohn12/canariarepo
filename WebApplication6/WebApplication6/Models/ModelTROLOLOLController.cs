﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication6.Models
{
    public class ModelTROLOLOLController : Controller
    {
        // GET: ModelTROLOLOL
        public ActionResult Index()
        {
            return View();
        }

        // GET: ModelTROLOLOL/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ModelTROLOLOL/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ModelTROLOLOL/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ModelTROLOLOL/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ModelTROLOLOL/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ModelTROLOLOL/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ModelTROLOLOL/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
