﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication6.Controllers
{
    public class TROLOLOLController : Controller
    {
        // GET: TROLOLOL
        public ActionResult Index()
        {
            return View();
        }

        // GET: TROLOLOL/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TROLOLOL/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TROLOLOL/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TROLOLOL/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: TROLOLOL/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TROLOLOL/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TROLOLOL/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
