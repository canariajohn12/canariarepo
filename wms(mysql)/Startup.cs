﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(wms_mysql_.Startup))]
namespace wms_mysql_
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

