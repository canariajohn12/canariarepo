//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace wms_mysql_.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class receiving
    {
        public int Control_ { get; set; }
        public Nullable<int> Item_ID { get; set; }
        public int Item_Item_ID { get; set; }
        public int Customer_Customer_ID { get; set; }
        public Nullable<long> Number_of_Items { get; set; }
        public string Package_Type { get; set; }
        public string Remarks { get; set; }
        public int Column_Column_ID { get; set; }
        public string Date { get; set; }
        public string column_name { get; set; }
    
        public virtual column column { get; set; }
        public virtual customer customer { get; set; }
        public virtual item item { get; set; }
    }
}
