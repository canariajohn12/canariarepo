﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using wms_mysql_.Models;

namespace wms_mysql_.Controllers
{
    public class putawaysController : Controller
    {
        private wms_final_databaseEntities27 db = new wms_final_databaseEntities27();

        // GET: putaways
        public ActionResult Index()
        {
            var putaways = db.putaways.ToList();
            return View(putaways.ToList());
        }

        // GET: putaways/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            putaway putaway = db.putaways.Find(id);
            if (putaway == null)
            {
                return HttpNotFound();
            }
            return View(putaway);
        }

        // GET: putaways/Create
        public ActionResult Create()
        {
            ViewBag.Receiving_Control_ = new SelectList(db.receivings, "Control_", "Batch_Code");
            return View();
        }

        // POST: putaways/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Item_ID,Customer_ID,Receiving_Control_")] putaway putaway)
        {
            if (ModelState.IsValid)
            {
                db.putaways.Add(putaway);
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }

            ViewBag.Receiving_Control_ = new SelectList(db.receivings, "Control_", "Batch_Code", putaway.Receiving_Control_);
            return View(putaway);
        }

        // GET: putaways/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            putaway putaway = db.putaways.Find(id);
            if (putaway == null)
            {
                return HttpNotFound();
            }
            ViewBag.Receiving_Control_ = new SelectList(db.receivings, "Control_", "Batch_Code", putaway.Receiving_Control_);
            return View(putaway);
        }

        // POST: putaways/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Item_ID,Customer_ID,Receiving_Control_")] putaway putaway)
        {
            if (ModelState.IsValid)
            {
                db.Entry(putaway).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Receiving_Control_ = new SelectList(db.receivings, "Control_", "Batch_Code", putaway.Receiving_Control_);
            return View(putaway);
        }

        // GET: putaways/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            putaway putaway = db.putaways.Find(id);
            if (putaway == null)
            {
                return HttpNotFound();
            }
            return View(putaway);
        }

        // POST: putaways/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            putaway putaway = db.putaways.Find(id);
            db.putaways.Remove(putaway);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Confirm(int id)
        {
            ordering or = new ordering();
            putaway putaway = db.putaways.Find(id);
            customer cs = db.customers.Find(putaway.Customer_ID);
            item it = db.items.Find(putaway.Item_ID);
            or.Customer_ID = putaway.Customer_ID;
            or.PutAway_Receiving_Control_= putaway.Receiving_Control_;
            or.Item_ID = putaway.Item_ID;
            or.Customer_Name = cs.Customer_Name;
            or.Item_Name = it.Item_Name;
            or.Number_of_Items = putaway.Number_of_Items;
            or.column_ID = putaway.column_ID;
            or.column_name = putaway.column_name;
            db.orderings.Add(or);
            db.SaveChanges();
            db.putaways.Remove(putaway);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
