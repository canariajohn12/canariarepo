﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using wms_mysql_.Models;

namespace wms_mysql_.Controllers
{
    public class orderingsController : Controller
    {
        private wms_final_databaseEntities27 db = new wms_final_databaseEntities27();

        // GET: orderings
        public ActionResult Index(string textbox)
        {
            var name = from m in db.orderings
                       where m.Customer_Name == textbox
                       select m;
            return View(name.ToList());
        }
        public ActionResult Confirm(picking pk, int id, string number_order)
        {   
            ordering order = db.orderings.Find(id);
            int number = unchecked((int)order.Number_of_Items);
            int numberoforders=int.Parse(number_order);
            number = number - numberoforders;
            char[] pwd = "1234567890987654321".ToCharArray();
            Random rnd = new Random();
            string id1 = "";
            for (int i = 0; i < 9; i++)
            {
                id1 += pwd[rnd.Next(0, 9)].ToString();
            }
             var controlnumber = Convert.ToInt32(id1);
            order.Number_of_Items = number;
            db.Entry(order).State = EntityState.Modified;
            pk.Customer_ID = order.Customer_ID;
            pk.Customer_Name = order.Customer_Name;
            pk.Item_ID = order.Item_ID;
            pk.Item_Name = order.Item_Name;
            if(numberoforders>number)
            {
                pk.Number_of_Items=number;
            }
            pk.Number_of_Items = numberoforders;
            pk.Ordering_PutAway_Receiving_Control_ = controlnumber;
            pk.column_ID = order.column_ID;
            pk.column_name = order.column_name;
            db.pickings.Add(pk);
            if(order.Number_of_Items<=0)
            {
                db.orderings.Remove(order);
            }
            db.SaveChanges();
           return RedirectToAction("Index");
        }

        // GET: orderings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ordering ordering = db.orderings.Find(id);
            if (ordering == null)
            {
                return HttpNotFound();
            }
            return View(ordering);
        }

        // GET: orderings/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: orderings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Customer_ID,Item_ID,PutAway_Receiving_Control_,Item_Name,Customer_Name,Number_of_order")] ordering ordering)
        {
            if (ModelState.IsValid)
            {
                db.orderings.Add(ordering);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ordering);
        }

        // GET: orderings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ordering ordering = db.orderings.Find(id);
            if (ordering == null)
            {
                return HttpNotFound();
            }
            return View(ordering);
        }

        // POST: orderings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Customer_ID,Item_ID,PutAway_Receiving_Control_,Item_Name,Customer_Name")] ordering ordering)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ordering).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ordering);
        }

        // GET: orderings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ordering ordering = db.orderings.Find(id);
            if (ordering == null)
            {
                return HttpNotFound();
            }
            return View(ordering);
        }

        // POST: orderings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ordering ordering = db.orderings.Find(id);
            db.orderings.Remove(ordering);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
