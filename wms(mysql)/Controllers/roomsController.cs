﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using wms_mysql_.Models;

namespace wms_mysql_.Controllers
{
    public class roomsController : Controller
    {
        private wms_final_databaseEntities27 db = new wms_final_databaseEntities27();

        // GET: rooms
        public ActionResult Index()
        {
            return View(db.rooms.ToList());
        }

        // GET: rooms/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            room room = db.rooms.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }
            return View(room);
        }

        // GET: rooms/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: rooms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Room_ID,Room_Name,X_Coordinate,Y_Coordinate,Height,Total_Location,Status")] room room)
        {
            if (ModelState.IsValid)
            {
                char[] pwd = "1234567890987654321".ToCharArray();
                Random rnd = new Random();
                string id = "";
                for (int i = 0; i < 9; i++)
                {
                    id += pwd[rnd.Next(0, 19)].ToString();
                }
                room.Room_ID = Convert.ToInt32(id);
                db.rooms.Add(room);
                db.SaveChanges();
                int x, y;
                int height=0;
                var X_Cor = room.X_Coordinate;
                var Y_Cor = room.Y_Coordinate;
                var H_Cor = room.Height;
                for (x = 1; x <=X_Cor;x++ )
                {

                    
                    //for(y=0;y<Y_Cor;y++)
                    //{   
                            
                            //for(height=0;height<H_Cor;height++)
                            //{

                            
                            location loc = new location();
                            string guid = System.Guid.NewGuid().ToString();
                            loc.Location_ID =guid;
                            loc.Room = room.Room_Name;
                            loc.X_Coordinate = x;
                            loc.Y_Coordinate = x;
                            height++;
                            loc.Height = height;
                            loc.Status = room.Status;
                            loc.Status = "latest";
                            db.locations.Add(loc);
                            db.SaveChanges();
                            //}

                    
                        
                    //}
                }
                    return RedirectToAction("Index");
            }

            return View(room);
        }

        // GET: rooms/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            room room = db.rooms.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }
            return View(room);
        }

        // POST: rooms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Room_ID,Room_Name,X_Coordinate,Y_Coordinate,Height,Total_Location,Status")] room room)
        {
            if (ModelState.IsValid)
            {
                db.Entry(room).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(room);
        }

        // GET: rooms/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            room room = db.rooms.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }
            return View(room);
        }

        // POST: rooms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            room room = db.rooms.Find(id);
            db.rooms.Remove(room);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
