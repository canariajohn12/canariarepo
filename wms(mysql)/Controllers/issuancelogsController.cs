﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using wms_mysql_.Models;

namespace wms_mysql_.Controllers
{
    public class issuancelogsController : Controller
    {
        private wms_final_databaseEntities27 db = new wms_final_databaseEntities27();

        // GET: issuancelogs
        public ActionResult Index()
        {
            return View(db.issuancelogs.ToList());
        }

        // GET: issuancelogs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            issuancelog issuancelog = db.issuancelogs.Find(id);
            if (issuancelog == null)
            {
                return HttpNotFound();
            }
            return View(issuancelog);
        }

        // GET: issuancelogs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: issuancelogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ControlNumber,Plate_Number,Temp,ContainerNumber,Customer_ID,Item_ID")] issuancelog issuancelog)
        {
            if (ModelState.IsValid)
            {
                db.issuancelogs.Add(issuancelog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(issuancelog);
        }

        // GET: issuancelogs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            issuancelog issuancelog = db.issuancelogs.Find(id);
            if (issuancelog == null)
            {
                return HttpNotFound();
            }
            return View(issuancelog);
        }

        // POST: issuancelogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ControlNumber,Plate_Number,Temp,ContainerNumber,Customer_ID,Item_ID")] issuancelog issuancelog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(issuancelog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(issuancelog);
        }

        // GET: issuancelogs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            issuancelog issuancelog = db.issuancelogs.Find(id);
            if (issuancelog == null)
            {
                return HttpNotFound();
            }
            return View(issuancelog);
        }

        // POST: issuancelogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            issuancelog issuancelog = db.issuancelogs.Find(id);
            db.issuancelogs.Remove(issuancelog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
