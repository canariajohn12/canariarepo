﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using wms_mysql_.Models;

namespace wms_mysql_.Controllers
{
    public class receivingsController : Controller
    {
        private wms_final_databaseEntities27 db = new wms_final_databaseEntities27();
        public ActionResult popup()
        {
            return View();
        }
        
            public ActionResult page()
        {
            return View();
        }
        public ActionResult table(string search,string textbox)
        {
            db.customers.ToList();
            return PartialView(db.customers.Where(x => x.Customer_Name == search).ToList());
        }
        // GET: receivings
        public ActionResult Index()
        {
            var receivings = db.receivings.ToList();
            return View(receivings.ToList());
        }
        public ActionResult Confirm(int id)
        {
            
            putaway pt = new putaway();
            receiving rc = db.receivings.Find(id);
            customer cs = db.customers.Find(rc.Customer_Customer_ID);
            column col = db.columns.Find(rc.Column_Column_ID);
            item it = db.items.Find(rc.Item_Item_ID);
            pt.Customer_ID = rc.Customer_Customer_ID;
            pt.Item_ID = rc.Item_Item_ID;
            pt.Receiving_Control_ = rc.Control_;
            pt.Customer_Name = cs.Customer_Name;
            pt.Item_Name = it.Item_Name;
            pt.Number_of_Items = rc.Number_of_Items;
            pt.column_ID = rc.Column_Column_ID;
            pt.column_name = rc.column_name;
            col.Column_Name = rc.column_name;
            col.Column_ID = rc.Column_Column_ID;
            db.putaways.Add(pt);
            db.SaveChanges();
            db.receivings.Remove(rc);
            db.columns.Remove(col);
            db.SaveChanges();
            return RedirectToAction("Create");
        }

        // GET: receivings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            receiving receiving = db.receivings.Find(id);
            if (receiving == null)
            {
                return HttpNotFound();
            }
            return View(receiving);
        }

        // GET: receivings/Create
        public ActionResult Create()
        {
            ViewBag.Column_Column_ID = new SelectList(db.columns.OrderBy(x=>x.Column_Name), "Column_ID", "Column_Name");
            ViewBag.Customer_Customer_ID = new SelectList(db.customers, "Customer_ID", "Customer_Name");
            ViewBag.Item_Item_ID = new SelectList(db.items, "Item_ID", "Item_Name");
            ViewBag.Control_ = new SelectList(db.putaways, "Receiving_Control_", "Receiving_Control_");
            return View();
        }

        // POST: receivings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Control_,Item_ID,Item_Item_ID,Customer_Customer_ID,Column_Column_ID,Package_Type,Remarks")] receiving receiving, putaway pw, string textbox, string textboxsub, string palletnumber,string Remarks,string Date)
        {
            if (ModelState.IsValid)
            {   
                
                    int palletnumber1 = int.Parse(palletnumber);
                    customer cs = db.customers.Find(receiving.Customer_Customer_ID);
                    item it = db.items.Find(receiving.Item_Item_ID);
                    column cl = db.columns.Find(receiving.Column_Column_ID);
                    int containernumber = int.Parse(textbox);
                    receiving.Number_of_Items = palletnumber1;
                    receiving.Control_ = containernumber;
                    receiving.Customer_Customer_ID = cs.Customer_ID;
                    receiving.Item_ID = it.Item_ID;
                    receiving.Remarks = Remarks;
                    receiving.Date = Date;
                    receiving.column_name = cl.Column_Name;
                    db.receivings.Add(receiving);
                    db.SaveChanges();

            }
            ViewBag.Column_Column_ID = new SelectList(db.columns.OrderBy(x => x.Column_Name), "Column_ID", "Column_Name",receiving.Customer_Customer_ID);
            ViewBag.Customer_Customer_ID = new SelectList(db.customers, "Customer_ID", "Customer_Name", receiving.Customer_Customer_ID);
            ViewBag.Item_Item_ID = new SelectList(db.items, "Item_ID", "Item_Name", receiving.Item_Item_ID);
            ViewBag.Control_ = new SelectList(db.putaways, "Receiving_Control_", "Receiving_Control_", receiving.Control_);
            return View();
        }

        // GET: receivings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            receiving receiving = db.receivings.Find(id);
            if (receiving == null)
            {
                return HttpNotFound();
            }
            ViewBag.Column_Column_ID = new SelectList(db.columns, "Column_ID", "Column_Name", receiving.Column_Column_ID);
            ViewBag.Customer_Customer_ID = new SelectList(db.customers, "Customer_ID", "Customer_Name", receiving.Customer_Customer_ID);
            ViewBag.Item_Item_ID = new SelectList(db.items, "Item_ID", "Item_Name", receiving.Item_Item_ID);
            ViewBag.Control_ = new SelectList(db.putaways, "Receiving_Control_", "Receiving_Control_", receiving.Control_);
            return View(receiving);
        }

        // POST: receivings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Control_,Batch_Code,Item_ID,Container_,Vehicle_Plate_,Item_Item_ID,Customer_Customer_ID,Column_Column_ID")] receiving receiving)
        {
            if (ModelState.IsValid)
            {
                db.Entry(receiving).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Column_Column_ID = new SelectList(db.columns, "Column_ID", "Column_Name", receiving.Column_Column_ID);
            ViewBag.Customer_Customer_ID = new SelectList(db.customers, "Customer_ID", "Customer_Name", receiving.Customer_Customer_ID);
            ViewBag.Item_Item_ID = new SelectList(db.items, "Item_ID", "Item_Name", receiving.Item_Item_ID);
            ViewBag.Control_ = new SelectList(db.putaways, "Receiving_Control_", "Receiving_Control_", receiving.Control_);
            return View(receiving);
        }

        // GET: receivings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            receiving receiving = db.receivings.Find(id);
            if (receiving == null)
            {
                return HttpNotFound();
            }
            return View(receiving);
        }

        // POST: receivings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            receiving receiving = db.receivings.Find(id);
            db.receivings.Remove(receiving);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
