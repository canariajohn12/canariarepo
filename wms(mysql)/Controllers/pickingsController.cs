﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using wms_mysql_.Models;

namespace wms_mysql_.Controllers
{
    public class pickingsController : Controller
    {
        private wms_final_databaseEntities27 db = new wms_final_databaseEntities27();

        // GET: pickings
        public ActionResult Index()
        {
            var pickings = db.pickings.ToList();
            return View(pickings.ToList());
        }
        public ActionResult Confirm(int id)
        {
            issuance issue = new issuance();
            picking pk = db.pickings.Find(id);
            issue.Customer_ID = pk.Customer_ID;
            issue.Customer_Name = pk.Customer_Name;
            issue.Item_ID = pk.Item_ID;
            issue.Item_Name = pk.Item_Name;
            issue.Picking_Ordering_PutAway_Receiving_Control_ = pk.Ordering_PutAway_Receiving_Control_;
            issue.Number_of_Items = pk.Number_of_Items;
            issue.column_ID = pk.column_ID;
            issue.column_name = pk.column_name;
            db.issuances.Add(issue);
            db.SaveChanges();
            db.pickings.Remove(pk);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        // GET: pickings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            picking picking = db.pickings.Find(id);
            if (picking == null)
            {
                return HttpNotFound();
            }
            return View(picking);
        }

        // GET: pickings/Create
        public ActionResult Create()
        {
            ViewBag.Ordering_PutAway_Receiving_Control_ = new SelectList(db.issuances, "Picking_Ordering_PutAway_Receiving_Control_", "Picker_Name");
            return View();
        }

        // POST: pickings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Customer_ID,Item_ID,Ordering_PutAway_Receiving_Control_")] picking picking)
        {
            if (ModelState.IsValid)
            {
                db.pickings.Add(picking);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Ordering_PutAway_Receiving_Control_ = new SelectList(db.issuances, "Picking_Ordering_PutAway_Receiving_Control_", "Picker_Name", picking.Ordering_PutAway_Receiving_Control_);
            return View(picking);
        }

        // GET: pickings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            picking picking = db.pickings.Find(id);
            if (picking == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ordering_PutAway_Receiving_Control_ = new SelectList(db.issuances, "Picking_Ordering_PutAway_Receiving_Control_", "Picker_Name", picking.Ordering_PutAway_Receiving_Control_);
            return View(picking);
        }

        // POST: pickings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Customer_ID,Item_ID,Ordering_PutAway_Receiving_Control_")] picking picking)
        {
            if (ModelState.IsValid)
            {
                db.Entry(picking).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Ordering_PutAway_Receiving_Control_ = new SelectList(db.issuances, "Picking_Ordering_PutAway_Receiving_Control_", "Picker_Name", picking.Ordering_PutAway_Receiving_Control_);
            return View(picking);
        }

        // GET: pickings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            picking picking = db.pickings.Find(id);
            if (picking == null)
            {
                return HttpNotFound();
            }
            return View(picking);
        }

        // POST: pickings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            picking picking = db.pickings.Find(id);
            db.pickings.Remove(picking);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
