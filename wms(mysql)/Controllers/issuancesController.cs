﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using wms_mysql_.Models;

namespace wms_mysql_.Controllers
{
    public class issuancesController : Controller
    {
        private wms_final_databaseEntities27 db = new wms_final_databaseEntities27();
        //public JsonResult GetStudents(string term)
        //{
        //    List<string> customers;
        //    customers = db.customers.Where(x => x.Customer_Name.StartsWith(term))
        //        .Select(y => y.Customer_Name).ToList();
        //    return Json(customers, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult Confirm(int id, int columnid, string columnname,column col)
        {
            issuance issue = db.issuances.Find(id);
            col.Column_ID = columnid;
            col.Column_Name = columnname;
            db.columns.Add(col);
            db.SaveChanges();
            //db.issuances.Remove(issue);
            db.SaveChanges();
            
            return View();
        }
        // GET: issuances
        public ActionResult Index()
        {
            var issuances = db.issuances.ToList();
            return View(issuances.ToList());
        }
        public ActionResult table(int id)
        {   
            //var issuances = db.issuances.ToList();
            var issuances = from m in db.issuances
                            where m.Picking_Ordering_PutAway_Receiving_Control_ == id
                            select m;

            return View(issuances.ToList());
        }

        // GET: issuances/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            issuance issuance = db.issuances.Find(id);
            
           
            if (issuance == null)
            {
                return HttpNotFound();
            }
            return View(issuance);
        }

        // GET: issuances/Create
        public ActionResult Create()
        {
            ViewBag.Picking_Ordering_PutAway_Receiving_Control_ = new SelectList(db.pickings, "Ordering_PutAway_Receiving_Control_", "Ordering_PutAway_Receiving_Control_");
            return View();
        }

        // POST: issuances/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Customer_ID,Item_ID,Picking_Ordering_PutAway_Receiving_Control_,Picker_Name,Plate_Number,Temp,ContainerNumber")] issuance issuance,int id)
        {
            if (ModelState.IsValid)
            {
                //db.issuances.Add(issuance);
                issuancelog issue = new issuancelog();
                issuance iss = db.issuances.Find(id);
                column col = new column();
                issue.ControlNumber = id;
                issue.Customer_ID = iss.Customer_ID;
                issue.Item_ID = iss.Item_ID;
                col.Column_ID = iss.column_ID;
                col.Column_Name = iss.column_name;
                db.columns.Add(col);
                db.issuancelogs.Add(issue);
                db.issuances.Remove(iss);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Picking_Ordering_PutAway_Receiving_Control_ = new SelectList(db.pickings, "Ordering_PutAway_Receiving_Control_", "Ordering_PutAway_Receiving_Control_", issuance.Picking_Ordering_PutAway_Receiving_Control_);
            return View(issuance);
        }

        // GET: issuances/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            issuance issuance = db.issuances.Find(id);
            if (issuance == null)
            {
                return HttpNotFound();
            }
            ViewBag.Picking_Ordering_PutAway_Receiving_Control_ = new SelectList(db.pickings, "Ordering_PutAway_Receiving_Control_", "Ordering_PutAway_Receiving_Control_", issuance.Picking_Ordering_PutAway_Receiving_Control_);
            return View(issuance);
        }

        // POST: issuances/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Customer_ID,Item_ID,Picking_Ordering_PutAway_Receiving_Control_,Picker_Name")] issuance issuance)
        {
            if (ModelState.IsValid)
            {
                db.Entry(issuance).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Picking_Ordering_PutAway_Receiving_Control_ = new SelectList(db.pickings, "Ordering_PutAway_Receiving_Control_", "Ordering_PutAway_Receiving_Control_", issuance.Picking_Ordering_PutAway_Receiving_Control_);
            return View(issuance);
        }

        // GET: issuances/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            issuance issuance = db.issuances.Find(id);
            if (issuance == null)
            {
                return HttpNotFound();
            }
            return View(issuance);
        }

        // POST: issuances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            issuance issuance = db.issuances.Find(id);
            db.issuances.Remove(issuance);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
