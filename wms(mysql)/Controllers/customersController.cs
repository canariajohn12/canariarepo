﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using wms_mysql_.Models;

namespace wms_mysql_.Controllers
{
    public class customersController : Controller
    {
        private wms_final_databaseEntities27 db = new wms_final_databaseEntities27();

        // GET: customers
        public ActionResult Index()
        {
            return View(db.customers);
        }
        [HttpPost]
        public ActionResult Index(string Search)
        {   List<customer> cus;
            if(string.IsNullOrEmpty(Search))
                {
                    cus = db.customers.ToList();
                    
                }
            else
            {
                cus = db.customers.Where(x => x.Customer_Name.StartsWith(Search)).ToList();
            }

            return View(cus);
        }
        
        public JsonResult Getcustomer(string term)
        {
            var cus = from s in db.customers
                      select s.Customer_Name;
            var namelist = cus.Where(n => n.ToLower().StartsWith(term.ToLower()));


            return Json(namelist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AutoCompleteCountry(string term)
        {
            var result = (from r in db.customers
                          where r.Customer_Name.ToLower().Contains(term.ToLower())
                          select new { r.Customer_Name }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        

        // GET: customers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            customer customer = db.customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: customers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Customer_ID,Customer_Name,Customer_Contact_Number,Email_address")] customer customer)
        {
            if (ModelState.IsValid)
            {
                char[] pwd = "1234567890987654321".ToCharArray();
                Random rnd = new Random();
                string id = "";
                for (int i = 0; i < 9; i++)
                {
                    id += pwd[rnd.Next(0, 9)].ToString();
                }
                customer.Customer_ID = Convert.ToInt32(id);
                db.customers.Add(customer);
                db.SaveChanges();
                return RedirectToAction("Create", "receivings");
            }

            return RedirectToAction("Create", "receivings");
        }

        // GET: customers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            customer customer = db.customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Customer_ID,Customer_Name,Customer_Contact_Number")] customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customer);
        }

        // GET: customers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            customer customer = db.customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            customer customer = db.customers.Find(id);
            db.customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
